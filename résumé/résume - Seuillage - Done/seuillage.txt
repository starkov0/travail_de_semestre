review -->
(Survey over image thresholding techniques and quantitative performance evaluation, Mehmet Sezgin, Bülent Sankur, Journal of Electronic Imaging 13(1), 146 – 165 (January 2004))



travail porte sur les techniques de ségmentation d'image
pas de "heuristic searching, simulated annealing, genetic algorithm"


plusieurs techniques pour améliorer visibilité dans l'image et faire ressortir les parties importantes sémantiquement
	égalisation d'histo -> améliorer image -> permet de mieux différencier certaines parties de l'image -> segmentation visuelle
	-> loi lagarithmique...


sueillage par image -> un seul seuil ou combinaison de seuils


un seuil par image
	image découpé en morceaux puis seuil sur chaque bout et ensuite surface de seuil
		(Computer Vision and Image Processing, 1992, Linda Shariro, Azriel Rosenfeld)



histogram ->
	analyse de la concavité et de la convexité de l histogramme, point le plus concave = seuil
	analyse de vallées -> convolution de l'histogramme avec noyau moyenneur (Gausse) puis déviateur (Laplace) -> seuil = entre premier bout traversant 0 et deuxième bout traversant 0
	shape-model threshold ?? (http://www.busim.ee.boun.edu.tr/~sankur/SankurFolder/Threshold_survey.pdf)

	(A Survey of Thresholding Techniques*, P. K. SAHOO, S. SOLTANI, AND A. K. C. WONG)
	augmentation des vallées dans l'histogramme :
	technique de Masson -> prendre : D = image dérivée, image Finale = imageNormal/(D^2 + imageNormal) -> pour chaque pixel
	-> amélioration de la possibilité de séparé l'histogramme en 2

	analyse de la courbure de l'histogramme -> fonction de distribution au poinr k : F(k) = sum[g 0->k](h(g))/sum[g 0->l-1](h(g)) / l-1 = nb de niveaux de gris
	coubure C(x) = F''(x)[1+(F'(x)^2)]^(-3/2) -> C(x) est pleine de bruit -> nécessaire de lisser
	zéros de courbure -> seuil
	(5. S. Boukharouba, J. M. Rebordao, and P. L. Wendel, An amplitude segmentation method based on the distribution function of an image, Comput. Vision Graphics Image Process. 29, 19S5,47-59.)
	
	prendre les pixels "edge" et les classifier sombre ou clair par rapport au voisinage
	faire un histogramme pour les clairs et un autre pour les foncés -> seuil = un des pics d un des 
	-> recursion : refaire le meme procédé avec les pixels au dessus de seuil puis ac pixel en dessous de seuil -> multiples seuils
	(S. Wang and R. M. Haralick, Automatic multithreshold selection, Comput. Vision Graphics Image Process. 25, 1984, 46-67.)




séparé l'image en blocks de niveaux de gris homogènes 
	-> objet dans image -> variance basse -> homogénéité du niveau de gris
	-> inhomogénéité -> variance grande
	-> séparer en parties de variance basse -> approximer ces parties par leurs moyennes
	-> faire histogramme de l'image ac les blocs de moyenne -> plus de vallées dans l'histogramme




	méthode de contraste uniforme : seuil doit détecter un max de "edge" ac haut contraste et min "edge" de bas contraste
	-> créer vecteur de mu(t) pour tout t (t appartenant aux niveaux de gris)
	-> mu(t) = C(t)/N(t) -> C(t) = contraste total de l'image / N(t) = nombre de "edge" détectés
	contraste -> plusieurs méthodes -> (différence de luminance)/(moyenne de luminance)
	appliqué recursivement (enlevé l'intensité des seuils précédents) -> multiples seuils 




clustering ->
	seuil itératif -> sur histogramme
	-> commencer par mettre T0 = mean(image), séparer histo en 2 groupes, faire moyenne sur chaque groupe -> Tn = (mu1 + mu2) / 2
	-> fin d itération lorsque |Tn - Tn+1| < epsilon, Tn = seuil entre foreground et background




	méthode d'Otsu -> trouver seuil qui maximise variance inter-classes et minimise variance intra-classe
	varianceT = variance totale
	computer la variance pour tous les seuils possibles [1..256]
	w0 = histFrequence(1:t)'*ones(t,1);
	w1 = histFrequence(t+1:end)'*ones(256-t,1);
	mu1 = histFrequence(t+1:end)'*(t+1:256)'/w1;
	mu0 = histFrequence(1:t)'*(1:t)'/w0;
	varianceB = w0*((mu0-moyenneTot)^2) + w1*((mu1-moyenneTot)^2);
	maximisation = varianceB/varianceT;
	(On Threshold Selection Using Clustering Criteria J. KITTLËR, MEMBER, IEEE, AND J. ILLINGWORTH)

	modification de otsu -> ne pas maximiser diff de moyenne mais diff de variance + ajouter facteur permettant d'être au milieux des deux pics
	w0 = histFrequence(1:t)'*ones(t,1);
	w1 = histFrequence(t+1:end)'*ones(256-t,1);
	mu1 = histFrequence(t+1:end)'*(t+1:256)'/w1;
	mu0 = histFrequence(1:t)'*(1:t)'/w0;
	variance0 = ((((1:t)-mu0).^2)*histFrequence(1:t)) / w0;
	variance1 = ((((t+1:256)-mu1).^2)*histFrequence(t+1:end)) / w1;
	maximisation = w0*((variance0-varianceT)^2) + w1*((variance1-varianceT)^2) * (1-histFrequence(t)/2);
	(An Improved Image Segmentation Algorithm Based on Otsu Method, WANG Hongzhi, DONG Ying, Changchun, jilin, China, 130012)

	Fuzzy clustering (logique floue) 
	-> grouper les points en clusters (f,b) en utilisant une fonction à minimiser
	MUf(x) = 1/(1+d(x,Cn)/d(x,Cb))^(2/tau - 1)
	MUb(x) = 1 - MUn(x)
	-> calculer les centres des groupes
	Mk = sum(x*p(x)*MUk(x))
	-> recommencer algol sauf si peu de mouvement des centres entre les itérations
	fonction de distance peut être soit fonction de distance euclidienne, soit fonction de probabilités
	(C. V. Jawahar, P. K. Biswas, and A. K. Ray, "Investigations on fuzzy thresholding based on fuzzy clustering", Pattern Recogn. 30(10),1605–1613, (1997))

entropy -> 
	maximisation de l'entrepie de l'image binaire -> max info
	minimisation de la cross-etropie entre l'image de départ et l'image binaire -> min perte d'info
	Entropic Yen -> -log(sum[0->T]((p(x)/p(T))^2)) -log(sum[T+1->X]((p(x)/(1-p(T)))^2) -> A miximiser
	Entropie Pun -> -sum[0->T]p(g)*log(p(g)) -sum[T+1->G]p(g)log(p(g)) -> A maximiser
	( T. Pun, "A new method for gray-level picture threshold using the entropy of the histogram", Signal Process. 2(3), 223–237, (1980))

	Cross-Entropie seuil -> distance de Kullback-Leibler -> D(q,p) = sum( q(g)*log(q(g)/p(g)) ) -> p(g) = image originale -> q(g) = image binaire -> A minimiser
	-> sum[0->T]( q(g)*log(q(g)/p(g))) + sum[T+1->G]( q(g)*log(q(g)/p(g))) -> A minimiser

	fuzzy entropie threshold -> plus la distance entre couleur et seuil est grande -> plus appartenance à un groupe est grande
	pour un niveau de gris d'une distance de i du seuil -> 
	mu0(T-i) = 05 + (p(T)+p(T-1)+...+p(T-i))/(2*p(t)) -> appartenance au background
	mu1(T+i) = 05 + (p(T)+p(T+1)+...+p(T+i))/(2*(1-p(t))) -> appartenance au foreground
	T -> min(H0(T)-H1(T)) -> H0 et H1 doivent être égales
	H0 = -sum[0->T](p(g)*log(mu0(g))/p(T))
	H1 = -sum[T+1->G](p(g)*log(mu1(g))/(1-p(T)))

étude de similarité (fuzzy shape similarity, edge coincidence) ->
	-> étude du "shape", compactness, gray-level moments, connectivity, texture, stabilité des objets segmentés
	-> ressemblance  entre image originale et binaire -> mesures floues, cumulative probabilité distribution

	moment preserving seuil -> moment = fonction de mesure quantitative sur un set de points
	-> max(mk - bk) 
	mk = sum(p(g)*(g^k))
	bk = Pf*(Mf^k) + Pb*(Mb^k)

	edge préservation seuil 
	-> utiliser opérateur différentiateur (Sobel) -> avoir une image dérivée
	diviser l'image originale en plusieurs blocs, tester plusieurs seuils par blocks
	-> seuil cherché = min(binaire(sobelImage) - seuilImage) -> préserver un maximum les contours dans l'image
	(Multilevel Thresholding Using Edge Matching, LOIS HERTZ AND RONALD W. SCHAFER, COMPUTER VISION, GRAPHICS, AND IMAGE PROCESSING 44, 279--295 (1988) )

	similarités floues seuil -> calcule de la distance floue entre chaque pixel et le foreground
	0 <= MUf[I(i,j)] <= 1 -> MUf[I(i,j)] = mesure floue pour le pixel (i,j) d'appartenir au foreground
	-> apres avoir trouvé mesure floue pour chaque pixel -> index pour toute l'image
	seuil optimal -> min de l'index pour toute l'image -> index : moyenne, medianne, logarithmic entropie

	topologic état stable -> placer seuil pour que foreground aille une taille correcte
	utilisation de la fonction : size-threshold function -> nombre d'objets possédant au moins "S" pixels
	seuil = milieux du plateau le plus grand de la fonction

	seuil maximum information -> poser seuil = changement d'incertitude dans l'image
	seuil idéal = min(H(p) - alpha*H(pf) - (1-alpha)*H(pb))
	H(p) = incertitude du pixel p dans image originale
	alpha = probabilité que qu'un pixel appartienne au foreground -> (fréquence de pixels foreground)/(tot pixels)

	augmentation de floue compactness seuil -> minimiser aire/périmètre^2 sur image binaire
	aire = intégrale de Mu -> sum[i,j](Mu(I(i,j))
	Mu = toute partie de l'image dont les pixels ne sont pas égals à 0
	périmetre = sum[i,j](abs(Mu(I(i,j) - Mu(I(i,j+1)) + sum[i,j](abs(Mu(I(i,j) - Mu(I(i+1,j))
	(Image enhancement and thresholding by optimization of fuzzy compactness, Sankar K. PAL* and Azriel ROSENFELD, Pattern Recognition Letters 7 (1988) 77-86)

	autre seuil floue -> texture, sémantique

spatial probabilité + correlation entre pixels ->
	analyse dépendance dans voisinage -> corrélation, cooccurrence, dépendance linéaire, entropie 2D

	p-tile -> dans le cas ou on connais le pourcentage d'occupation d un objet dans une image -> si objet est majoritaire 
	-> on prends le niveau de gris le plus fréquent comme seuil

	seuil de cooccurrence -> image avec histogramme identique peuvent avoir une structure spatiale différente
	-> analyse des probabilités de valeurs de gris dans un voisinage horizontal et vertical
	calculer cooccurrence de l'image à niveaux de gris : C[m,n] = (1/R) * sum[i,j](P(I(i,j) == m) & P(I(i±1,j±1) == n))
	-> features textiles importants = Energy, Entropy, Contrast, Variance, Correlation, Inverse Difference Moment, Edge Magnitude(grey pair value difference -> plus on s'éloigne de la diagonal, plus elle augmente)
	matrice de cooccurrence -> quatre partie -> A = gray level transition dans foreground, D =  gray level transition dans background, B+C = boundary
	-> utiliser cooccurrence pour calculer entropie
	-> seuil optimal -> max( entropie(A) + entropie(D) ), max( entropie(B) + entropie(C) ), max( entropie(A) + entropie(D) + entropie(B) + entropie(C) )
	-> seuil optimal -> min( entropie relative(image originale, image binaire) )
	seuil optimal -> max( contraste quantification )
	contraste quantification = (1/n) * sum[0->L-p][m+p->L](((m+n)/2)*C'(m,n)) -> n = sum[0->L-p][m+p->L](C'(m,n)) -> calcule sur partie triangulaire sup à partir de p -> object boundary
	(Adaptive Thresholding Based On Co-Occurrence Matrix Edge Information, M. M. Mokji, S.A.R. Abu Bakar, JOURNAL OF COMPUTERS, VOL. 2, NO. 8, OCTOBER 2007)
	seuil -> distance Kullback-Leibler min entre cooccurrence originale et cooccurrence binaire

	-> coccurrence -> faire 2 histogrammes -> premier -> proche de la diagonale de cooccurrence, second -> loins de la diagonale 
	-> seuil choisis au sein des valeurs ou le premier histo met une vallée (différence entre objets) et second possède un pic (différence entre objets)

	higher-order entropie -> creation de histogramme 2D -> comme cooccurrence mais -> prendre pixel et moyenne de voisinage(3x3) -> C(m,n) = pixel de couleur m entouré par voisinage moyen de couleur n
	-> trouvé seuil -> appliqué discriminant linéaire de Fisher -> séparation des données en 2 groupes avec min(intra-variance) et max(inter-variance)
	(Thresholding based on Fisher linear discriminant, Gamil Abdel-Azim, Z.A.Abo-Eleneen, Journal of Pattern Recognition Research 2 (2011) 326-334)
	seuil optimal -> max(min(Hf(T,TT),Hb(T,TT))) -> Hf = - sum([0->T](sum[0->TT]((p(g,gg)/p(T,TT))*log(p(g,gg),p(T,TT)))) -> gg = moyenne voisinage g, TT = moyenne voisinage T
	-> diminution de l'incertitude au sein des classes et augmentations de l'information entre classes
	(Using spatial information as an aid to maximum entropy image threshold selection, A.D. Brink, Pattern Recognition Letters 17 (1996) 29-36)
	seuil -> découpé histo 2D en quatre blocks -> 2 seuils (S,T) -> seuil optimal -> somme d'entropie des blocs max
	(Thresholding Using Two-Dimensional Histogram Based on Local Entropy, Zhao Cheng, Tianxu Zhang, Luxin Yan, 978-1-4244-5194-4/10/$26.00 ©2010 IEEE)

	Fixed Block Method -> diviser l'image binaire en blocks sxs -> classifier chaque type de bloc par sa structure -> besoins de matrices de taille 2^(s*s) pour classifier les structures des blocs
	-> seuil optimal -> entropie max sur blocs
	(A. Beghdadi, A. L. Negrate, and P. V. De Lesegno, ‘‘Entropic thresholding using a block source model, "Graph. Models Image Process. 57 , 197–205 (1995))

	moving block Method -> fenêtre de taille sxs se ballade sur l'image -> répertorié les blocs par rapport au nombre de pixels blancs qu'ils contiennent -> indifférent aux structures
	entropyMax pour bloc de sxs -> loi uniforme -> log(s^2 + 1) 
	entropie = sum[0->s^2](Pk(s,t)*log(Pk(s,t)))
	vrai entropie = entropie/entropieMax
	à cause de temps de calcule -> prendre t appartenant à [tmin,tmax] et s appartenant à [2,sMax], sMax est d'habitude L/2
	-> seuil optimal = max(vrai entropie(s,t)) -> t
	(A. Beghdadi, A. L. Negrate, and P. V. De Lesegno, ‘‘Entropic thresholding using a block source model, "Graph. Models Image Process. 57 , 197–205 (1995))

	random set method : -> basé sur histogramme -> connaitre la distribution probe / + basé sur distance -> basé sur spatial
	-> random set -> utiliser moyenne -> si utiliser la moyenne de distance -> phénomène spatial pris en compte
	g(.) = histogramme / G(.) = histogramme cumulatif / mu = moyenne / W = image ou ensemble aléatoire -> chaque variable appartiens à [0-255] / G(.)/mu(W) = distribution cumulative / g(.)/mu(W) = distribution densité
	W -> [0:255] / f:W -> [0,1] / U = ensemble formé de [0,1]
	égalisation d'histo -> améliorer image -> permet de mieux différencier certaines parties de l'image -> segmentation visuelle -> ne fonctionne pas par recherche du minimum local si histo unimodal
	espérance de vorobev = espérance des moyenne de sets -> permet de donner une caractéristique quant à un set de variables aléatoires -> l espérance de variables aléatoires est importante dans la théorie des variables aléatoires
	espérance de vorobev = E(mu(X)) = intégrale sur image originale de P(x) dx -> intégrale de histogramme de image originale.
	-> moyenne de vorobev ne donne pas d'information sur la nature spatial du set aléatoire -> sum([0:255].*(histogram(image)/(longueur*largeur)))
	distance euclidienne = d(x,X) = infinimum{dist(x,y): y appartiens à X}, x appartiens à Rm -> distance minimale entre tout point de x et tout point de y -> distance minimale entre les deux vecteurs
	distance signée = d(x,X) = 0 si x fit partie de X / -dist(x,X) x< min(X) ou x>max(x) / dist(x,X) si min(X)<x<max(x)
	distance carée = d(x,X) = dist²(x,X)
	-> dist n'est pas nécessairement dans une métrique euclidienne
	espérance de la distance = E(d(x,X)) = D(x) -> prendre en compte des données spatiales dans une moyenne
	-> trouver le x qui aproxime le mieux l espérance de la distance
	-> distance moyenne Xbarre est le X qui minimise ||D(.) - d(.,X)|| -> norme 1,2,inf
	(A new thresholding technique based on random sets, Nial Friel, Ilya S. Molchanov, Pattern Recognition 32 (1999) 1507-1517)

	2D-fuzzy partitionning : 
	-> utiliser histogramme 2D -> entrées = f(x,y) x g(x,y) -> f(x,y) = niveau de gris sur image à (x,y) / g(x,y) = bottom((1/4)*(f(x,y+1)+f(x,y-1)+f(x+1,y)+f(x-1,y))+0.5)
	-> utiliser fonction d'appartenance muA(x) = S(x,a,b,c) = 0 si x<=a / (x-a)^2/((b-a)*(c-a)) si a<=x<=b / 1 - ((x-c)^2/((c-b)*(c-a))) si b<=x<=c / 1 si c<=x  --> b = (c+a)/2
	fonction Z(x,a,b,c) = 1 - S(x,a,b,c)
	fonction d'appartenance de deux variables à un ensemble R formé de deux ensembles A et B : muR(x,y) = min(muA(x),muB(y))
	entropie fuzzy = H(A) = -sum[i 1->n](muA(xi)*p(xi)*log(p(xi))) -> auteur (L.A. Zadeh, Probability Measures of fuzzy events, J. Math. Anal. Appl., 23 (1968) 421—427)
	entropie non-fuzzy = H(A,P) = -sum[i 1->N](p(xi)*log(p(xi)))
	seuil idéale = max(H(A) + H(B)) -> max différentiation entre background et frontground -> computer sur toutes valeurs de "a" et de "c" -> b = (c+a)/2
	créer deux ensembles Bright et Dark -> nécessite de créer quatre ensembles -> BrightX BrightY DarkX DarkY
	muBright(x) = S(x,a,b,c) / muDark(X) = Z(x,a,b,c) / BX = sum[x dans X](S(x,a,b,c)/x) / DX = sum[x](Z(x,a,b,c)/x) / BY = sum[y](S(y,a,b,s)) / DY = sum[y](Z(y,a,b,c)/y)
	muBright(x,y) = min(muBrigthX(x),muBrightY(y)) / muDark(x,y) = min(muDarkX(x),muDarkY(y))
	BlockB (background) = Rb + R1 -> Rb = tout (x,y) ou muDark(x,y) = 1 (non-fuzzy) / R1 = tout (x,y) ou muDark(x,y) < 1 (fuzzy)
	BlockW (background) = Rw + R2 -> Rw = tout (x,y) ou muBright(x,y) = 1 (non-fuzzy) / R2 = tout (x,y) ou muBright(x,y) < 1 (fuzzy)
	H(BlockB) = Hfuzzy(R1) + Hnon-fuzzy(Rb) / H(BlockW) = Hfuzzy(R2) + Hnon-fuzzy(Rw)
	H(image) = H(BlocB) + H(BlocR)
	-> trouver meilleur découpage de l'histo 2D -> permet de séparer au mieux les blocs Bright et Dark -> récupérer les valeurs "a" et "c" -> seuil "t" et "s" entre valeurs "a" et "c" -> meilleur délimitation de région fuzzy
	utiliser seuil : 
	-> fT(x,y) = g0 si f(x,y) < t / g1 si f(x,y) >= t
	-> fS,T(x,y,bright) = g1 si f(x,y) >= t and g(x,y) >= s / g0 sinon
	-> possible de transformer en algo génétique
	(Fuzzy partition of two-dimensional histogram and its application to thresholding, H.D. Cheng, Yen-Hung Chen, Pattern Recognition 32 (1999) 825—843)

variation du seuil local ->
	(seuil calcul pour chaque pixel en fonction de voisinage -> statistique, variance, surface-fitting)

	local contrast methods : 
	pixel T(i,j) -> moyenne dans voisinage 15x15 -> 
	si T(i,j) > moyenne -> object / sinon background
	-> autre alternative -> seuil = moyenne de min + max de voisinage / sauf si max-min < 15 -> à ce moment la voisinage fait partie d une seule classe -> dépends de la valeur de T(i,j)

	surface-fitting method :
	filtre passe-bas sur l'image
	-> gradient de l'image filtré = G 
	-> segmenter G par un seuil pour avoir des points de repère sur l'image 
	-> utiliser ces points de repère sur l'image originelle pour créer une surface interpolée qui passe par ces points -> seuiller
	-> si objets de foreground et de background ressortent -> éliminer quelque surfaces quand à leur moyenne
	(A New Method for Image Segmentation, S. D. YANOWITZANDA. M. BRUCKSTEIN, FacultyofElectricalEngineering,Technion,IIT, 32000 Haifa,Israel)

	basé sur le niveau de gris locale -> n = max niveau de gris dans bloc 5x5
	-> si n <= 40 : pixel appartiens au label objet si g(pixel) - g(n) < tau -> sinon appartiens au label background
	si n > 40 : pixel appartiens au label objet si g(pixel) < g(n)/mu -> sinon appartiens au background
	pixels concidérés : [0,n,n,n,0;n,n,0,n,n;n,0,X,0,n;n,n,0,n,n;0,n,n,n,0]
	(B. Bhanu and 0. Faugeras, Segmentation of images having unimodal distributions, IEEE Trans. Pultern Anui. Mach. Intell. PAMI-4, 1982, 408-419.)

	-> diviser l'image en blocs 7x7
	-> lorsque histogramme de bloc = bimodal -> approximer par somme de gaussiennes -> mettre seuil de manière à diminuer erreur de classification
	-> lorsque histogramme de bloc = unimodal -> approximer seuil par interpolation de seuils de blocks voisins
	(C. K. Chow and T. Kaneko, Boundary detection of radiographic images by a threshold method, in Proceedings, ZFZP Congress 71, pp. 130-134.)

	diviser image en blocs -> trouver seuil par entropie de Pun -> filtre passe bas sur les frontères des blocs

	(A Survey of Thresholding Techniques*, P. K. SAHOO, S. SOLTANI, AND A. K. C. WONG)
	méthode de la relaxation -> (relaxation = technique reccursive de convergence de systèmes linéaires)
	1 -> classifier les pixels par leur probe d'appartenir au clair ou au foncé
	2 -> itérativement ajuster la probe de chaque pixel par la proba du voisinage
	3 -> arrêter itérations lorsque les pixels clairs ont une grande proba d a'appartenir au clair
	(A. Rosenfeld and R. C. Smith, §	§, IEEE Trans. Pattern Anal. Mach. Intell. PAMI-3, 1981, 598-606.)
	classification -> L = valeur max claire / D = valeur min de foncé / G = niveau de gris du pixel / -> ProbaD = (L-G)/(L-D) / ProbaL = (G-D)/(L-D)
	-> ne marche pas si niveau de gris objet et background pas très différent -> autre solution ->
	M = mean(niveau de gris) / if G > M -> PL = 1/2 + (1/2)*(G-M)/(L-M) / else -> PD = 1/2 + (1/2)*(M-G)/(M-D)
	update probabilities ->  Ri,j(lambda,lambdaprim) = -1 si lambda et lambdaprim sont incompatibles / 0 si xi et jx sont indépendants / 1 si lambda et lambdaprim sont compatibles -> lambda = label de xi, lambdaprim = label de xj
	N = 8 voisins du pixel xi
	-> Pk+1,i(lambda) = Pk,(lambda)*(1+Qk(lambda))/(sum[lambda,lambdabis](Pk(lambda)*(1+Qk(lambda))) / Qk(lambda) = (1/8)*sum[xj dans N](sum[lambda,lambdabis](Ri,j(lambda,lambdabis)Pk(lambdabis)))
	meilleur équation : Pk+1,i(lambda) = Pk,i(lambda)*sum[xj dans N](sum[lambda,lambdabis](Ri,j(lambda,lambdabis)Pk(lambdabis)))/(sum[lambda](Pk,i(lambda)*sum[xj dans N](sum[lambda,lambdabis](Ri,j(lambda,lambdabis)Pk(lambdabis))))
	(R. Southwell, Relaxation Methoak in Engineering Science, A Treatise on Approximate Computatron, Oxford Univ. Press, London, 1940)
	
	méthode de relaxation par gradient : lambda1 = label light / lambda2 = label dark / [Pi(lambda1), Pi(lambda2)]T = set de probe associés aux niveaux de gris / [Qi(lambda1), Qi(lambda2)] = set de vecteurs de compatibilité
	-> Qi(lambdak) = (1(8)*sum[x dans N](Pi(lambdak)) / N = 8 voisins de xi
	-> trouver les meilleurs labels en maximisant la fonction : C℗ = sum[i dans niveaux de gris](Pi(lambda)T*Qi(lambda)T)
	-> Pi(lambda) = P(lambda1),P(lambda2) sachant que P(lambdaI) >= 0, sum[j dans 1,2](P(lambdaj)) == 1
  	-> maximiser C(P) -> réduction de l'ambiguité
	-> sinon -> maximiser par rapport à la théorie de l'information -> maximiser psi(P) = sum[i dans niveaux de gris](I(Pi,Qi))
	-> I(P(i),Q(i)) = sum[j dans 1,2](Pi(lambdaj)*log(Pi(lambdaj)/Qi(lambdaj)))
	(B. Bhanu and 0. Faugeras, Segmentation of images having unimodal distributions, IEEE Trans. Pultern Anui. Mach. Intell. PAMI-4, 1982, 408-419.)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Basés sur les métaheuristiques ->



	Artificial Bee Colony Algorithm :
	toute source de nourriture est considéré comme solution à un problème spécifique / quantité de nourriture de chaque source de nourriture = fonction objective
	trois types d'abeilles : employées, onlookers, scouts
	plusieurs cycles -> les employées sont envoyés pour mesurer la quantité de nectar des sources -> les sources de nectar sont choisis par les onlookers -> les scouts vont trouver les nouvelles sources de nectar
	-> créer aléatoirement une population X = {xi} -> calculer le fitness F = {fi de xi} et mémoriser le max fi ainsi que la source correspondante
	-> chaque employé crée une nouvelle solution vi = xi + (xi - xk)*r -> k est un entier proche de i (pas égale à i) / r = [-1,1]
	-> calculer le fitness de vi -> si (fitness de vi) > fi -> xi = vi / sinon on garde xi
	-> proba Pi en rapport à fi = (fi*ti)/sum[i 1->n](fi*ti)
	-> fi*ti = 1/(1+fi) if fi >= 0 / fi*ti = 1 + abs(fi) if fi <= 0
	-> en prennent compte de Pi -> onlookers choisissent la source de de nectar -> cherchent le voisinage pour créer les nouvelles solutions -> calculent les nouveaux fitnes fi
	-> update des sources par méthode greedy
	-> mémorisation de la source contenant le plus de nectar ainsi que le montant de nectar et check si il y des solutions qu on a manqué
	-> si il existe d autres solutions -> les remplacer par une génération aléatoire xi = min +(max-min)*r' -> r' = [0,1] / min et max est le "bound" de de solutions possibles
	-> arrêt des itérations si le nombre d'itérations max est dépassé ou si la condition d'arrêt est satisfaite
	


	--> image décomposé par décomposition d'ondelette de 3 niveaux 
	-> 3ème partie low fréquence (information approximative) utilisé pour reconstruire l'image approximative
	-> 3ème partie high fréquence (edge et texture) utilisé pour reconstruire image de gradient
	-> filter low pass sur image approximé -> image filtré
	-> image filtré G et gradient I sont normalisés -> [0-255]
	-> création de matrice 256x256 de co-occurence C gradient-filtre -> improved two dimensional entropy :
		I(m,n) = round(abs(L(m,n)/max[m,n](L(m,n))))*(L-1) 
		G(m,n) = round(abs(G(m,n)/max[m,n](G(m,n))))*(L-1)
	-> improved two dim entropy = fitness function de ABS
	-> construction de C[LxL] : C(i,j) = nombre d'occurence de I(m,n) = i and G(m,n) = j
	-> proba de C(i,j) = P(i,j) = C(i,j)/sum[i,j 0->L-1](C(i,j))
	-> C peut être divisé en 4 parties par seuil de I et de G -> Q1,Q2;Q4,Q3 -> Q1 = objet / Q4 = background / Q2 = edge et texture dans objet / Q3 = edge et texture dans background
	-> Shannon conditionnelle entropie par bloc : 
		H(E|O) = sum[i 0->S](sum[j t+1->L-1](P(i,j)Q2 * log2(1/P(i,j)Q2)))
		H(E|B) = sum[i S+1->L](sum[j t+1->L-1](P(i,j)Q3 * log2(1/P(i,j)Q3)))
	-> max H(E|O) et H(E|B) -> meilleur seuil pour I et G
	meilleur seuil = H(s,t) = 1/2*(H(E|O) + H(E|B))



	--> fuzzy c-mean cluster : Jfcm(U,V) = sum[k = 1->n](sum[i 1->c](U[ik m]d2(Xk,Vi)) d2 = distance euclidienne / n = nombre de variables / c = nb de clusters
		U[ik m] = appartenance de degré m du pixel k au cluster i / d2(Xk,Vi) = distance euclidienne entre le pixel k et le centre du cluster i
	FCM-ABC -> cluster de départ est une solution optimale dans les solutions clusterisées
	(Brain Tumor Segmentation In MRI Image Using Unsupervised Artificial Bee Colony And FCM Clustering, Neeraja R Menon ,M Karnan ,R Sivakumar,International Journal of Computer Science and Management Research Vol 2 Issue 5 May 2013)

	algo génétique :
	(A Methodologica l Survey and Proposed Algorithm on Image Segmentation using Genetic Algorithm, Megha Agarwal, Vijai Singh, International Journal of Computer Applications (0975 – 8887) Volume 67 – No.1 6 , April 2013)


	









