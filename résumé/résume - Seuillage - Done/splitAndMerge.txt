split and merge : -> pas besoins d'interaction -> peut être automatisé
	définir critère d homogénéité
	cassé image en 4 quadrants -> utiliser une représentation en arbre quaternaire pour concasser image
	pour chaque quadrant, si pas d homogénéité -> recommencé au point 2
	les quadrants satisfaisant l'homogénéité -> les rassembler -> utiliser un arbre d'adjacence pour pour rassembler les sous-images entre elles
	-> rassembler deux noeuds = détruire ces deux noeuds et le remplacer par un autre
	-> rassembler en séquentiel -> une région risque de trop grandir et de dépasser les limites fuzzy de l'objet
	-> rassembler en parallèle -> moins d'erreur de dépassement des limites fuzzy
	-> si région trop petite -> tenter de l'incorporé à une région voisine 
	-> possible d'utiliser écart-type comme critère d'homogénéité
	(AUTOMATED DETECTION OF EXUDATES IN RETINAL IMAGES USING A SPLIT- AND-MERGE ALGORITHM, Hussain F. Jaafar, Asoke K. Nandi and Waleed Al-Nuaimy, 18th European Signal Processing Conference (EUSIPCO-2010))
	(A Summary of Image Segmentation Techniques, Lilly Spirkovska, Ames Research Center, Moffett Field, California ,NASA Technical Memorandum 104022)


region growing : -> besoins d'une interaction pour guider le début du merge
	choisir pixel appartenant à une région à segmenté -> faire grandir la région en absorbant les pixels qui sont autour
	-> si région claire -> prendre les pixels voisins d'intensité maximale
	-> arrêt de l'agrandissement de la région lorsque le nombre de pixels absorbés dépasse une certains valeur
	-> autre moyen s'arrête l'agrandissement -> lorsque la moyenne de contraste de la boundary atteinds maximum
	-> boundary contrats = différence entre pixel interne frontière et pixel externe frontière
	-> maximiser le contraste entre les régions -> permet de différencier au mieux différents objets 
	(Region Growing: A New Approach, S. A. Hojjatoleslami and J. Kittler)

region growing example :
	tumeur du sein 
	(CLASSIFICATION OF BREAST LESIONS BASED ON QUANTITATIVE MEASURES OF TUMOR MORPHOLOGY Scott Pohlman', Kimerly Powell ,Nancy Obuchowski , William Chilcote ,Sharon Grundfest-Broniatowski, 1995 IEEE-EMBC and CMBEC)

basé sur la connectévité : utilisation de données anatomiques à des fins de segmentation -> exemple sur une vue médio sagittale d'une tête
	-> binarisé l'image par un seuil
	-> éliminer les trous dans l'image
	-> trouvé pointe du nez + la partie horizontalement contraire au nez sur la partie postérieur du crâne + partie inférieur postérieur du crane
	-> création de trous aux trois niveaux trouvés
	-> éliminer toutes les petites régions non-connexes -> ne garder que la plus grande : obligatoirement cerveau -> partie supérieur -> LCR entoure cerveau -> permet d'éliminer le crâne sup. en créant les trous
	-> problème pour séparer partie inférieur du cerveau -> partie très tissulaire -> méthodes de k-means et gradient ne permettent pas séparer correctement.

	-> utiliser algorithme de Lee -> permet de trouver un chemin entre deux points si il existe
		-> entouré la région de séparation par un carré -> appliqué itérativement un seuil jusqu'à trouver un chemin permettant la démarcation entre les deux parties (chemin de couleur plus foncée)
		-> ne pas accepté chemin si longueur trop grande ou pas assez lisse
		-> difficulté de trouver chemin dans une image de texture et bruité
	(Unsupervised connectivity-based thresholding segmentation of midsagittal brain MR images, Chulhee Lee a, Shin Huh a , Terence A. Ketter b , Michael Unser, Computers in Biology and Medicine 28 (1998) 309-338)