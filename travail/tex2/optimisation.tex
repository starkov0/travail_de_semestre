\chapter{Métaheuristiques et optimisations}\label{optimisation}
Une métaheuristique est une heuristique de haut niveau, souvent basés sur des notions probabilistes.
\\Les algorithmes, présentés dans cette section, utilisent des métaheuristiques pour la recherche d'un seuil optimal de segmentation, pour une analyse de texture ou pour d'autres règles de segmentation.

	% cooccurrence - gradient
	\section{Cooccurrence - Gradient}
La méthode, ci-dessous, utilise l'algorithme ABC [\ref{ABC}] et permet de trouver un seuil optimal de segmentation.
\\On procède, tout d'abord, à la création de deux images $G$ et $W$. $G$ est l'image gradient normalisé. Pour la création de $W$, on calcule la décomposition en wavelettes à trois niveaux de l'image originale, on récupère la partie haute fréquence de la décomposition, et on recalcule l'image originale grâce à cette partie uniquement. L'image $W$ correspond à l'image reconstruite après un filtrage passe-bas.
\\On crée ensuite une matrice de cooccurrence $C$ [\ref{cooccurrence}] que l'on rempli de la manière suivante :
\begin{align*}
& C(i,j) = \\
& \text{\small{nombre d'occurrences de }} G(\cdot,\cdot) = i \text{ et } W(\cdot,\cdot) = j
\end{align*}
La probabilité $\Pr$, de la matrice $C$, est calculé grâce à :
$$
\Pr(i,j) = \frac{C(i,j)}{\sum\limits_{i,j}C(i,j)}
$$
La matrice $C$ est divisé en quatre blocs $Q1$, $Q2$, $Q3$ et $Q4$ grâce au seuillage des matrices $G$ et $W$.

\begingroup
    \centering
    \includegraphics[scale=0.8]{../img/ABC-Entropie.png}
    \captionof{figure}{Matrice de cooccurrence\cite{ABC1-seuil}.}
\endgroup

$Q1$ correspondent aux pixels des objets de l'image, $Q4$ correspond au fond de l'image, $Q2$ correspond aux contours et aux textures des objets, et $Q3$ correspond aux textures et aux contours du fond.
Dans le but de trouver le seuil optimal à la segmentation, on utilise l'algorithme ABC [\ref{ABC}] avec comme fonction de fitness la maximisation de la moyenne des entropies des blocs $Q2$ et $Q3$. \cite{ABC1-seuil}.
\\
\\Une autre méthode, utilisant l'algorithme ABC, consiste à appliquer un algorithme de classification [\ref{classifieurs}] sur la matrice de cooccurrence $C$ [\ref{kMoyennes}] \cite{FCMABC-seuil}.

	% relaxation
	\section{Relaxation Stochastique}
Les algorithmes d'optimisation pour la classification des textures sont nombreux. Une grande partie se base sur la méthode de relaxation stochastique [\ref{relaxationStoch}]. Ces méthodes nécessite une fonction $f$ pour la quantification d'un état de segmentation. Cette fonction travaille, souvent, soit sur la partition des régions, soit sur la détection des frontières.
\\Dans la partition des régions, on vérifie que les régions segmentées sont homogènes et qu'elles comportent, un minimum de pixels. Dans la détection des frontières, on vérifie que les frontières de segmentations correspondent aux contours des objets existants dans l'image.
\\L'algorithme de recuit simulé\footnote{simulated anealing} \cite{anealing-texture} et l'algorithme de segmentation d'image par contraintes d'optimisation\footnote{constrained optimization image segmentation algorithm, COIS} sont tous deux basés sur ces méthodes. 

	% Perceptron
	\section{Perceptron}
Le réseau de neurone de type Perceptron avec et sans rétro-propagation sont utiles en segmentation d'image, lorsqu'ils sont combinés avec des algorithmes à algèbre floue ou des algorithmes génétiques \cite{perceptron}.

	% Hopfield
	\section{Hopfield}
L'utilisation des réseaux de Hopfield [\ref{hopfield}] pour la segmentation d'image nécessite une fonction d'énergie, ainsi qu'un nombre de classes $k$ prédéfinis. Le $k$ optimale se trouve dans l'intervalle [7-8]. La fonction d'énergie, à minimiser, inclut une mesure de distance entre les centres de classes et les pixels leur appartenant. Les distances euclidiennes ou de Mahalanobis sont souvent utilisées \cite{hist-coocc-texture-hopfield}.
\\La méthode débute par la linéarisation de l'image en un vecteur de taille $n$. On crée, ensuite, une matrice de taille $[n \times k]$. Les lignes représentent les pixels. Les colonnes représentent les classes. 
\\La segmentation se termine lorsque chaque ligne possède une seule valeur 1 et que le reste des valeurs est un 0. De cette manière, les pixels sont mono-classifiés à 100\%.

\begingroup
    \centering
    \includegraphics[scale=0.9]{../img/hopfieldMatrixClassification.png}
    \captionof{figure}{Segmentation d'images à l'aide d'un réseau de Hopfield. Chaque ligne possède une seule valeur 1 et le reste des valeurs sont des 0 \cite{hist-coocc-texture-hopfield}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.3]{../img/hopfieldUse1.png}
    \captionof{figure}{Image d'exemple pré-segmentation \cite{hist-coocc-texture-hopfield}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.9]{../img/hopfieldUse2.png}
    \captionof{figure}{Image segmentée à l'aide d'un réseau de Hopfield \cite{hist-coocc-texture-hopfield}.}
\endgroup

	% kohonen
	\section{Kohonen}
Les réseaux de Kohonen [\ref{kohonen}] sont utilisés dans la segmentation d'images dans un but d'optimisation. Du fait de la projection des données sur le réseau, différents types de données nécessitent l'utilisation de différentes topologies de réseau.
\\Dans le cas où l'on utilise un prétraitement par seuillage, un réseau à topologie en anneaux est préféré. En effet, la projection sur ce type de réseau permet de récupérer une structure fermée reproduisant les contours de l'objet segmenté \cite{kohonenUse}.

\begingroup
    \centering
    \includegraphics[scale=0.55]{../img/kohoneneUse1.png}
    \captionof{figure}{(à gauche) Image originale affichée en 3 dimensions. (à droite) Image résultante d'un cueillage de l'image originale  \cite{kohonenUse}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.4]{../img/kohoneneUse2.png}
    \captionof{figure}{Segmentation des contours de l'image seuillée grâce à un réseau de Kohonen circulaire \cite{kohonenUse}.}
\endgroup

Dans le cas où on s'intéresse aux textures ou aux structures complexes de l'image, un réseau sous forme de grille est utilisé \cite{kohonenUse}.

\begingroup
    \centering
    \includegraphics[scale=0.7]{../img/kohoneneUse3.png}
    \captionof{figure}{Mapping de l'image originale grâce à un réseau de Kohonen sous forme de grille \cite{kohonenUse}.}
\endgroup

La classification post-apprentissage est basée sur la distance minimale entre les pixels des nouvelles images et les neurones du réseau \cite{kohonenUse2}.
























