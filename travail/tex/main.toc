\enlargethispage {\baselineskip }
\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {paragraph}{Suite du travail}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Outils math\IeC {\'e}matiques}{4}{chapter.2}
\contentsline {section}{\numberline {I}Statistiques}{4}{section.2.1}
\contentsline {paragraph}{Entropie}{4}{section.2.1}
\contentsline {paragraph}{Matrice de cooccurrence}{4}{section.2.1}
\contentsline {paragraph}{Histogramme 2D}{4}{section.2.1}
\contentsline {section}{\numberline {II}Structures}{4}{section.2.2}
\contentsline {paragraph}{Analyse Fractale}{4}{section.2.2}
\contentsline {section}{\numberline {III}Spectres}{5}{section.2.3}
\contentsline {paragraph}{D\IeC {\'e}composition de Fourrier}{5}{section.2.3}
\contentsline {paragraph}{D\IeC {\'e}composition en wavelette}{5}{section.2.3}
\contentsline {paragraph}{Filtre de Gabor}{5}{figure.2.2}
\contentsline {section}{\numberline {IV}Classifications}{5}{section.2.4}
\contentsline {paragraph}{Algorithme des k-moyennes}{5}{section.2.4}
\contentsline {paragraph}{Discriminant lin\IeC {\'e}aire de Fisher}{5}{section.2.4}
\contentsline {paragraph}{Arbres de d\IeC {\'e}cision}{6}{figure.2.4}
\contentsline {paragraph}{Support vector machine}{6}{figure.2.4}
\contentsline {paragraph}{R\IeC {\'e}seaux de neurones artificels}{6}{figure.2.5}
\contentsline {paragraph}{Perceptron}{6}{figure.2.6}
\contentsline {paragraph}{R\IeC {\'e}seau de Hopfield}{6}{figure.2.6}
\contentsline {paragraph}{R\IeC {\'e}seau de Kohonen}{6}{figure.2.6}
\contentsline {section}{\numberline {V}M\IeC {\'e}taheuristiques et optimisations}{7}{section.2.5}
\contentsline {paragraph}{Artificial Bee Colony Algorithm}{7}{section.2.5}
\contentsline {paragraph}{Relaxation stochastique}{7}{section.2.5}
\contentsline {chapter}{\numberline {3}Seuillage}{8}{chapter.3}
\contentsline {section}{\numberline {I}Histogrammes}{8}{section.3.1}
\contentsline {paragraph}{\IeC {\'E}tude de concavit\IeC {\'e}}{8}{figure.3.1}
\contentsline {paragraph}{Analyse de vall\IeC {\'e}e}{8}{figure.3.1}
\contentsline {paragraph}{Analyse des contours}{8}{figure.3.1}
\contentsline {paragraph}{Contrastes uniformes}{8}{figure.3.1}
\contentsline {paragraph}{Optimisations}{8}{figure.3.1}
\contentsline {subparagraph}{Optimisation par les contours}{9}{figure.3.1}
\contentsline {subparagraph}{Optimisation par l'homog\IeC {\'e}n\IeC {\'e}it\IeC {\'e}}{9}{figure.3.3}
\contentsline {section}{\numberline {II}Clustering}{9}{section.3.2}
\contentsline {paragraph}{Seuillage it\IeC {\'e}ratif}{9}{section.3.2}
\contentsline {paragraph}{M\IeC {\'e}thode d'Otsu}{9}{equation.3.2.2}
\contentsline {paragraph}{M\IeC {\'e}thode d'Otsu modifi\IeC {\'e}e}{10}{equation.3.2.2}
\contentsline {paragraph}{Cross-entropie}{10}{equation.3.2.2}
\contentsline {paragraph}{Entropie floue}{10}{equation.3.2.2}
\contentsline {section}{\numberline {III}Similarit\IeC {\'e}s}{10}{section.3.3}
\contentsline {paragraph}{Pr\IeC {\'e}servation du moment}{10}{section.3.3}
\contentsline {paragraph}{Pr\IeC {\'e}servation des contours}{10}{section.3.3}
\contentsline {paragraph}{Similarit\IeC {\'e}s floues}{10}{section.3.3}
\contentsline {paragraph}{\IeC {\'E}tat topologique stable}{11}{section.3.3}
\contentsline {paragraph}{Similarit\IeC {\'e}s d'incertitude}{11}{section.3.3}
\contentsline {paragraph}{Compacit\IeC {\'e} floue}{11}{section.3.3}
\contentsline {paragraph}{Probabilit\IeC {\'e}s Spatiales et Corr\IeC {\'e}lation Inter-Pixels}{11}{section.3.3}
\contentsline {paragraph}{Analyse p-tile}{11}{section.3.3}
\contentsline {paragraph}{Cooccurrence - Entropie}{11}{section.3.3}
\contentsline {paragraph}{Cooccurrence - Contraste}{11}{figure.3.4}
\contentsline {paragraph}{Cooccurrence - Histogramme}{12}{figure.3.4}
\contentsline {paragraph}{Histogramme 2D - Fisher Linear Discreminant}{12}{figure.3.4}
\contentsline {paragraph}{Histogramme 2D - Entropie}{12}{figure.3.4}
\contentsline {paragraph}{Histogramme 2D - Entropie floue}{12}{figure.3.4}
\contentsline {paragraph}{Blocs fixes}{12}{figure.3.4}
\contentsline {paragraph}{Blocs mouvants}{12}{figure.3.5}
\contentsline {paragraph}{Ensembles al\IeC {\'e}atoires}{13}{figure.3.5}
\contentsline {section}{\numberline {IV}Variation du seuil local}{13}{section.3.4}
\contentsline {paragraph}{Contraste locale}{13}{section.3.4}
\contentsline {paragraph}{Surface de fitting}{13}{section.3.4}
\contentsline {paragraph}{Niveau de gris locale}{13}{figure.3.8}
\contentsline {paragraph}{Approximation Gaussienne}{13}{figure.3.8}
\contentsline {paragraph}{Entropie Locale}{14}{figure.3.9}
\contentsline {paragraph}{M\IeC {\'e}thode de la relaxation}{14}{figure.3.9}
\contentsline {paragraph}{Algorithme de Lee}{14}{figure.3.9}
\contentsline {section}{\numberline {V}Exemples d'applications : Segmentation Osseuse}{14}{section.3.5}
\contentsline {chapter}{\numberline {4}Analyse de texture}{16}{chapter.4}
\contentsline {section}{\numberline {I}Analyse structurelle}{16}{section.4.1}
\contentsline {paragraph}{Box-counting}{16}{section.4.1}
\contentsline {paragraph}{Mouvement Brownien}{16}{section.4.1}
\contentsline {section}{\numberline {II}Analyse statistique}{16}{section.4.2}
\contentsline {paragraph}{Classificaiton statistique}{16}{section.4.2}
\contentsline {paragraph}{Classification spectrale}{17}{figure.4.1}
\contentsline {paragraph}{Fourrier - segmentation}{17}{figure.4.1}
\contentsline {paragraph}{Wavelette - segmentation}{17}{figure.4.1}
\contentsline {paragraph}{Gabor - segmentation}{17}{figure.4.1}
\contentsline {section}{\numberline {III}Exemples d'applications : Classification des carcinomes pulmonaires}{17}{section.4.3}
\contentsline {chapter}{\numberline {5}Analyse r\IeC {\'e}gionale}{18}{chapter.5}
\contentsline {section}{\numberline {I}Split and merge}{18}{section.5.1}
\contentsline {section}{\numberline {II}Region growing}{18}{section.5.2}
\contentsline {section}{\numberline {III}Exemples d'applications : Segmentation des cavit\IeC {\'e}s cardiaques}{18}{section.5.3}
\contentsline {chapter}{\numberline {6}Atlas-Based}{20}{chapter.6}
\contentsline {section}{\numberline {I}Simples}{20}{section.6.1}
\contentsline {section}{\numberline {II}Statistiques}{20}{section.6.2}
\contentsline {section}{\numberline {III}Exemples d'applications : Segmentation c\IeC {\'e}r\IeC {\'e}brale et cardiaque}{20}{section.6.3}
\contentsline {chapter}{\numberline {7}M\IeC {\'e}taheuristiques et optimisations}{22}{chapter.7}
\contentsline {section}{\numberline {I}Cooccurrence - Gradient}{22}{section.7.1}
\contentsline {section}{\numberline {II}Relaxation Stochastique}{22}{section.7.2}
\contentsline {section}{\numberline {III}Perceptron}{22}{section.7.3}
\contentsline {section}{\numberline {IV}Hopfield}{22}{section.7.4}
\contentsline {section}{\numberline {V}Kohonen}{23}{section.7.5}
\contentsline {chapter}{\numberline {8}R\IeC {\'e}sum\IeC {\'e} et Conclusions}{24}{chapter.8}
\contentsline {paragraph}{R\IeC {\'e}sum\IeC {\'e}}{24}{chapter.8}
\contentsline {paragraph}{Conclusions}{24}{chapter.8}
\contentsline {chapter}{Bibliographie}{25}{chapter.8}
