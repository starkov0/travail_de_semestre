\chapter{Seuillage}\label{seuillage}
La segmentation par seuillage travail au niveau des intensités de couleur des pixels et consiste à trouver une valeur de couleur permettant de séparer, au mieux, les pixels d'une image \cite{survey-seuil-tot}. La séparation est définit par :
$$
ImgSegm(i,j) = \left\{
    \begin{array}{ll}
        1 \mbox{si Image(i,j) > seuil}\\
        0 \mbox{sinon}
    \end{array}
\right.
$$
$ImgSegm$ est l'image segmentée et $Image$ est l'image originale. 
\\La segmentation optimale permet de séparer les parties sémantiquement importantes, en retournant une image binaire. 
\\Il est possible d'utiliser un seuil unique ou un set de seuils. Le set de seuil permet d'agrandir la palette de couleurs au niveau de l'image retournée.
\\Il est possible d'utiliser un seuil unique par image ou une surface de seuil\footnote{un seuil par pixel}. La surface est réalisée grâce à l'interpolation de seuils locaux et elle est de même taille que l'image originale \cite{seuil-surf}.
	
\section{Histogrammes}
Les méthodes, ci-dessous, utilise l'histogramme d'une image pour la recherche du seuil, optimisant la séparation des pixels. Cette méthode est basée sur des histogrammes bimodaux. Le seuil théorique se trouve dans le creux séparant les deux maximas. L'efficacité de ces méthodes est réduite pour d'autres types d'histogramme. 

\begingroup
    \centering
    \includegraphics[scale=0.65]{../img/bimodalHisto.jpg}
    \captionof{figure}{Histogramme bimodale \cite{bimodalHisto_IMG}}
\endgroup

	% concavité
	\paragraph{Étude de concavité}
La valeur seuil est le point le plus concave de l'histogramme \cite{cocavite-seuil}. 
\\Le calcule de ce point utilise la fonction de distribution de probabilité normalisée ci-dessous. $H$ est l'histogramme de l'image.
$$
F(k) = \frac{\sum\limits_{i=0}^k H(i)}{\sum\limits_{i \in couleurs} H(i)}
$$
La fonction de courbature se calcule grâce à :
$$
C(x) = \frac{F''(x)}{1+F'(x)^2)^{3/2}}
$$
$C(\cdot)$ est une fonction bruité et doit être lissée. Pour cela, on moyenne les fonctions $F'(\cdot)$ et $F''(\cdot)$ dans leur voisinage puis on fitte $C(\cdot)$ en utilisant la méthode des moindres carrés et le polynôme de \textit{Chebyshev} \cite{courbature-seuil}.
\\Le zéro de la fonction de courbature indique le seuil optimal. 

	% vallée
	\paragraph{Analyse de vallée}
On convolue, tout d'abord, l'histogramme avec un noyau moyenneur\footnote{noyau Gaussien, par exemple}, afin de lisser l'histogramme, puis avec un noyau déviateur\footnote{noyau Laplacien, par exemple} afin d'obtenir la dérivée seconde de l'histogramme. La valeur seuil optimal se trouve entre les deux passages par zéro de la dérivée seconde \cite{valle-seuil}.

	% Analyse des contours
	\paragraph{Analyse des contours}
Dans cette méthode on cherche la valeur seuil en analysant les pixels formant les contours des objets présents dans l'image \cite{contours-seuil}. 
\\On récupére les pixels des contours des objets de l'image, et on leurs associe une valeur. Cette valeur est calculée grâce à l'intensité moyenne de leur voisinage au sein de l'image originale. Ces valeurs permettent de séparer les pixels de contours en deux groupes différents. Le seuil optimal est trouvé en calculant le maxima de l'histogramme de l'un des deux groupes \cite{survey-seuil-tot}.

	% contraste uniforme
	\paragraph{Contrastes uniformes}
L'idée de cette méthode est de détecter un maximum de contours à haut contraste et un minimum de contours à bas contraste \cite{survey-seuil-tot}. 
\\Pour le calcule du contraste, on utilise la loi de \textit{Weber-Fechner}. 
\\$n$ est la taille de la palette de couleurs de l'image, $C(t)$ est le contraste de l'image segmentée en utilisant le seuil $t$, $N(t)$ étant le nombre de contours détectés sur l'image segmentée en utilisant le seuil $t$. 
\\On crée un vecteur $\mu$ de longueur $n$ et on applique la fonction ci-dessous.
$$
\mu[t] = \frac{C(t)}{N(t)}
$$
Le seuil optimal se trouve au maxima du vecteur $\mu$.

	% Optimisation
	\paragraph{Optimisations}
Quelques méthodes permettant d'optimiser la recherche du seuil en accentuant la séparabilité des pixels au sein de l'histogramme.

		% Optimisation des contours
		\subparagraph{Optimisation par les contours}
Cette technique consiste à diminuer l'intensité des pixels faisant partie des contours des objets et à accentuer le caractère bimodal de l'histogramme \cite{survey-seuil-Masson}. 
\\$G$ est le gradient de l'image originale\footnote{le gradient est calculé en convoluant l'image originale avec un noyau de Sobel ou de Prewit, par exemple} et $ImgOri$ est l'image originale.
\\L'image résultant $ImgRes$ est calculée grâce à :
$$
ImgRes(i,j) = \frac{ImgOri(i,j)}{ImgOri(i,j) + G(i,j)^2}
$$

\begingroup
    \centering
    \includegraphics[scale=0.6]{../img/beforGradient.png}
    \captionof{figure}{Image précédent le calcule du grandient $G$.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.6]{../img/AfterGradient.png}
    \captionof{figure}{Image gradient $G$.}
\endgroup

		% analyse de l'homogénéité
		\subparagraph{Optimisation par l'homogénéité}
Pour cette méthode, on utilise la variance comme valeur d'homogénéité. 
\\On sépare l'image en blocs homogènes et pour chaque bloc, on calcule une moyenne. On crée ensuite un histogramme en utilisant les moyennes calculées. L'histogramme des moyennes permet d'améliorer la séparabilité des pixels \cite{survey-seuil-tot}.

\section{Clustering}
Les méthodes de clustering utilisent des mesures probabilistes pour regrouper les pixels en clusters. Voici quelques exemples d'algorithmes :

	% seuillage itératif
	\paragraph{Seuillage itératif}
Le seuillage itératif est une méthode basé essentiellement sur le calcule des moyennes et de la convergence \cite{iterative-clustering-seuil}. 
\\$H$ est l'histogramme de l'image. Le seuil de départ $T_{0}$ est la moyenne de l'image. 
\\Les étapes itérative de l'algorithme sont les suivantes : 
\begin{align}
\begin{subarray}{l} \text{\small{ séparer les pixels de l'image en deux }} \\
	\text{\small{ ensembles, $E_{1}$ et $E_{2}$, en utilisant le seuil $T_{n-1}$ }} \end{subarray} \\
T_{n} = \frac{mean(E_{1}) + mean(E_{2})}{2}
\end{align}
La condition d'arrêt de l'algorithme est :  
$$
\abs{T_{n} - T_{n-1}} < \epsilon
$$

	% methode d'Otsu
	\paragraph{Méthode d'Otsu}
L'idée de la méthode d'Otsu est de séparer les pixels en deux classes tout en maximisant la variance inter-classes et en minimisant la variance intra-classe \cite{otsu-kittler-seuil} \cite{otsu-seuil}.
\\Pour illustrer le fonctionnement de cette méthode, il est nécessaire de définir quelque variables. 
\\$w_{0}$ et $w_{1}$ sont les sommes des fréquences des pixels de la première et de la seconde classe respectivement. $\mu_{0}$ et $\mu_{1}$ sont les intensités moyennes de la première et de la seconde classe. $\mu_{tot}$ est la moyenne des intensités de l'image. $H$ est l'histogramme de l'image. $t$ $\in$ ensemble des seuils, $n$ est égale à la taille de la palette de couleurs.
\begin{gather*}
w_{0} = \sum\limits_{i=0}^{t-1} H(t) \\
w_{1} = \sum\limits_{i=t}^{n-1} H(t) \\
\mu_{0} = \sum\limits_{i=0}^{t-1} \frac{H(t)}{w_{0}} \\
\mu_{1} = \sum\limits_{i=t}^{n-1} \frac{H(t)}{w_{1}} \\
\mu_{tot} = \sum\limits_{i \in couleurs} H(t)
\end{gather*}
$varOtsu_{0}$ et $varOtsu_{0}$ sont les variances intra-classes.
\begin{gather*}
varOtsu_{0} = w_{0}(\mu_{0}-\mu_{tot})^2 \\
varOtsu_{1} = w_{0}(\mu_{1}-\mu_{tot})^2
\end{gather*}
$var_{tot}$ est la variance des intensités de l'image.
$$
var_{tot} = \sum\limits_{i \in couleurs} (i-\mu_{tot})^2
$$
\\La méthode d'Otsu recherche itérativement le seuil $t$ qui maximise la fonction ci-dessous.
$$
\frac{varOtsu_{0} + varOtsu_{1}}{var_{tot}}
$$
Cette méthode se base donc principalement sur la différence des moyennes $\mu_{0}-\mu_{tot}$ et $\mu_{1}-\mu_{tot}$.

	%methode d'Otsu modifiée
	\paragraph{Méthode d'Otsu modifiée}
A la différence de la méthode d'Otsu originale, la méthode d'Otsu modifiée se base sur la différence des variances \cite{otsu-modif-seuil}.
\\Pour cette méthode, on définit $var_{0}$ et $var_{1}$ comme étant la variance d'ordre 2 des classes.
\begin{gather*}
var_{0} = \sum\limits_{i=0}^{t-1} (i-\mu_{0})^2 \\
var_{1} = \sum\limits_{i=t}^{n-1} (i-\mu_{1})^2
\end{gather*}
La fonction à maximiser, pour la méthode d'Otsu modifiée, est donc :
$$
w_{0}(var_{0}-var_{tot})^2 + w_{1}(var_{1}-var_{tot})^2) \left(1-\frac{H(t)}{2} \right)
$$

	% cross-entropy
	\paragraph{Cross-entropie}
La divergence de Kullback-Leibler est une mesure de la différence entre deux distributions de probabilité $p$ et $q$. Plus les distributions sont proches, et plus la divergence calculée est basse. Voici sa formule :
$$
D(q,p) = \sum\limits_{g=0}^{n-1} q(g)log \left( \frac{q(g)}{p(g)} \right)
$$
Dans le contexte de la segmentation d'images, $p$ représente l'image originale et $q$ représente l'image binarisée.
Le seuil $t$ idéale est trouvé, en minimisant la formule suivante \cite{survey-seuil-tot} :
$$
\sum\limits_{g=0}^{t} q(g)log \left( \frac{q(g)}{p(g)} \right) +  \sum\limits_{g=t+1}^{n-1} p(g)log \left( \frac{p(g)}{q(g)} \right)
$$

	% fuzzy entropie
	\paragraph{Entropie floue}
L'entropie flou se base sur l'idée qu'en choisissant un seuil quelconque pour une image, plus les couleurs seront éloignés du seuil, plus leur appartenance à un groupe sera grande \cite{survey-seuil-tot}.
\\Pour une binarisation à seuil $t$, on définit l'appartenance, d'une couleur $i$, au premier et au second groupe, $\mu_{0}$ et $\mu_{1}$, par la formule ci-dessous. $p(t)$ est la probabilité de la couleur $t$ au sein de l'image originale.
\begin{gather*}
\mu_{0}(t-i) = \frac{0.5 + (p(t)+p(t-1)+...+p(t-i))}{2p(t)} \\
\mu_{1}(t+i) = \frac{0.5 + (p(t)+p(t+1)+...+p(t+i))}{2(1-p(t))}
\end{gather*}
La mesure de l'entropie de chacun des groupes, $H_{0}$ et $H_{1}$ est calculé grâce à :
\begin{gather*}
H_{0} = -\sum\limits_{g=0}^t \frac{p(g)log(\mu_{0}(g))}{p(t)} \\
H_{1} = -\sum\limits_{g=t+1}^G \frac{p(g)log(\mu_{1}(g))}{1-p(t)}\\
g \in \text{couleurs de l'image}
\end{gather*}
Le seuil optimal est trouvé en minimisant la fonction ci-dessous.
$$
H_{0}(t)-H_{1}(t) 
$$
En minimisant la formule ci-dessus, on conditionne une similarité d'entropies pour les deux groupes.

\section{Similarités}
Les algorithmes basés sur l'étude des similarités travaillent au niveau de la préservation des ressemblances entre l'image originale et l'image binaire. Les ressemblances sont décrites sous différentes formes. 

	% preservation de moment
	\paragraph{Préservation du moment}
Le moment d'une variable aléatoire est une mesure utilisée en théorie des probabilités. Elle donne une information quant à la dispersion de cette variable.
\\L'algorithme de segmentation se basant sur la préservation du moment définit une méthode pour le calcule du moment de l'image originale $m_{k}$, et le moment de l'image binaire $b_{k}$ \cite{moment-seuil}. 
\begin{gather*}
m_{k} = \sum\limits_{g \in couleurs} \Pr(g)g^k \\
b_k = p_1y_1^k + p_2y_2^k
\end{gather*}
$p(g)$ est la probabilité de la couleur $g$ au sein de l'image originale. Pour le calcule de $b_{k}$, on définit un seuil que l'on utilise pour la segmentation de l'image. $y_1$ et $y_2$ sont les pixels qui sont plus petits et plus grands que le seuil, respectivement, et $p_1$ et $p_2$ sont leurs probabilité.
\\La préservation de plusieurs moments améliore la similitude entre l'image originale et l'image binaire. Habituellement, les ordres supérieurs à trois ne sont pas utilisés.
\\Le seuil idéale est trouvé en minimisant :
 $$
 m_k - b_k
 $$
 
 	% préservation des contours
	\paragraph{Préservation des contours}
La méthode de préservation des contours réalise des segmentations par seuillage en maximisant la différentiation entre les différents objets contenus dans l'image. En préservant les contours réels au sein de l'image segmenté, cette dernière maximise la sémantique contenue dans l'image originale \cite{survey-seuil-tot}. L'algorithme procède de la manière suivante :
\begin{itemize}[$\bullet$]
\item \text{On calcule le gradient de l'image\footnote{par convolution avec un noyau de Sobel, par exemple}.}
\item \text{On divise l'image en blocs.}
\item \text{Pour chaque bloc : }
	\begin{itemize}
	\item \text{\small{On teste plusieurs seuils}}
	\item \text{\small{On choisis celui qui minimise la différence entre :}}
	\end{itemize}
\end{itemize}
$$
\text{\small{le gradient binarisé et l'image segmentée}}
$$

	% similarités floues
	\paragraph{Similarités floues}
La méthode des similarités se base sur la minimisation d'un indexe de distance entre un seuil et un ensemble des pixels d'une image \cite{survey-seuil-tot}. De nombreux indexes sont utilisables, comme par exemple la moyenne, la médiane et l'entropie logarithmique. Le calcule des distances floues est ensuite utilisé. Il varie généralement entre $0$ et $1$.
\\Le seuil idéale est trouvé grâce à la minimisation de l'index de distance entre les pixels de l'image et le seuil choisis.

	% état topologique stable
	\paragraph{État topologique stable}
La méthode de l'état topologique stable est basé sur le fait que les objets contenus dans l'image originale sont habituellement compactes et possèdent au minimum $s$ pixels. En ajustant le seuil de manière croissante, on remarque que le nombre de plages compactes possédant au minimum $s$ pixels varie de manière non-linéaire et possède des plateaux de longueur variable. Le seuil idéale est choisis en prenant le milieux du plateau le plus large \cite{survey-seuil-tot}.

	% similarités d'incertitude
	\paragraph{Similarités d'incertitude}
La méthode des similarités d'incertitude est basée sur la mesure de l'entropie de l'image avant et après segmentation. 
\\L'entropie $H(X)$ de l'image originale $X$, est calculée grâce à  : 
$$
H(X) = - \sum\limits_{i \in couleurs} p_ilog(p_i)
$$
\\L'entropie de l'image après segmentation est défini par $H(X|C)$, où $C$ représente les classes de segmentation : 
$$
H(X|C) = P_0H(X|X \rightarrow C_0) + P_1H(X|X \rightarrow C_1)\footnote{$P_0$ et $P_1$ sont les fréquences des pixels de la première et de la seconde classe. $H(X|X \rightarrow C_0)$ et $H(X|X \rightarrow C_1)$ sont les entropie des pixels après classification.}
$$ 
Le seuil idéale est trouvé en maximisant : $H(X) - H(X|C)$. De cette manière, la quantité d'information, après classification, est minimalement réduite. Ce critère se nomme le \textit{Maximal Segmented Image Information} (MSII) \cite{similarite-incertitude-seuil}.
\\L'algorithme de \textit{Kittler-Illingworth} et le \textit{MSII thresholding algorithm} sont des exemples de techniques utilisant ce critère. 
\\Dans ces algorithmes, les variables de classe sont présumés suivre une loi normale. Ainsi, en analysant les moyennes et les écart-types, il est possible de calculer les densités de probabilité des variables de classes.
\\La \textit{Segmented Image Information} (SII) est ensuite calculé grâce aux entropies de densité de probabilité. Le seuil optimal se trouve au maxima de la SII.

	% compacité floue
	\paragraph{Compacité floue}
Cet algorithme se base sur la maximisation des aires compactes \cite{compactness-seuil}. L'idée principale est de trouvé le seuil qui minimise, pour toute surface non-nulle $\mu$ de l'image $I$, la formule ci-dessous.
$$
\text{\small{aire}}/\text{\small{périmètre}}^2
$$
On minimise ainsi les surfaces poreuses de grandes tailles, car elles représentent souvent une erreur de classification.
\\L'air et le périmètre sont trouvés de la manière suivante :
\begin{gather*}
\text{\small{Aire : }} \\
\sum\limits_{i,j} \mu(I(i,j)) \\
\text{\small{Périmètre : }} \\
\sum\limits_{i,j} \abs{\mu(I(i,j))-\mu(I(i,j+1))} + \abs{\mu(I(i,j))-\mu(I(i+1,j))}
\end{gather*}

\paragraph{Probabilités Spatiales et Corrélation Inter-Pixels}
Les méthodes développées dans cette section se basent essentiellement sur le calcule des densités de probabilités jointes pour réaliser une analyse spatiale de la distribution des couleurs au sein des pixels de l'image.

	% analyse p-tile
	\paragraph{Analyse p-tile}
L'analyse p-tile est une analyse basée sur la fraction d'occupation d'un objet au sein d'une image \cite{ptile-seuil}. 
\\Dans le cas où la fraction d'occupation $f$ est connue et que l'objet à segmenter est plus clair que le fond de l'image, le seuil optimale $t$ est trouvé grâce aux formules ci-dessous.\begin{gather*}
f \leq \frac{\sum\limits_{i=t}^{n-1} H(i)}{\sum\limits_{i=0}^{n-1} H(i)} \\
f \geq \frac{\sum\limits_{i=st+1}^{n-1} H(i)}{\sum\limits_{i=0}^{n-1} H(i)}
\end{gather*}
$H(\cdot)$ est l'histogramme de l'image originale. 

	% cooccurrence - Entropie
	\paragraph{Cooccurrence - Entropie}
Une matrice de cooccurrence [\ref{cooccurrence}] permet de détecter le seuil optimale en passant par le calcule de l'entropie\cite{cooccurrence-seuil}.
\\Pour cela, on divise la matrice en quatre partie $A$, $B$, $C$ et $D$ grâce à un seuil $t$.

\begingroup
    \centering
    \includegraphics[scale=0.8]{../img/cooccurrence.png}
    \captionof{figure}{Matrice de cooccurrence \cite{cooccurrence-seuil}.}\label{fig:a}
\endgroup

Chaque partie de la matrice de cooccurrence correspond à des structures spatiales différentes. $A$ représente les pixels l'objet contenue dans l'image, $D$ représente le fond de l'image, et $B$ et $C$ représentent les transitions entre l'objet et le fond de l'image.
\\Le seuil optimal est trouvé en maximisant la fonction ci-dessous.
$$
\text{\small{entropie(A) + entropie(B) + entropie(C) + entropie(D)}}
$$

	% Cooccurrence - Contraste
	\paragraph{Cooccurrence - Contraste}
La matrice de cooccurrence [\ref{cooccurrence}] permet de segmenter une image en utilisant la quantification de contraste \cite{cooccurrence-seuil}. Cette méthode se base sur l'idée que la segmentation optimale permet de maximiser le contraste entre les objets présents dans l'image.
\\Le seuil $t$ est alors calculé itérativement et son maxima correspond au seuil optimal :
\begin{gather*}
t = \sum\limits_{m=0}^{L-p} \sum\limits_{n=m+p}^{L} \frac{\frac{m+n}{2}C[m,n]}{\tau} \\
\tau = \sum\limits_{m=0}^{L-p} \sum\limits_{n=m+p}^{L} C[m,n]
\end{gather*}

	% cooccurrence - histogramme
	\paragraph{Cooccurrence - Histogramme}
Il est possible de construire une multitude d'histogramme en utilisant les diagonales d'une matrices de cooccurrence [\ref{cooccurrence}]. Les diagonales regroupent différentes informations. Celles proches du milieux de la matrice sont basées sur les pixels à voisinage homogène, alors que celles éloignées du milieux de la matrice sont basées sur les pixels à voisinage inhomogène \cite{survey-seuil-tot}.
\\Pour la recherche d'un seuil optimal, on crée donc deux histogrammes. Le premier est basé sur la diagonale médiale de la matrice de cooccurrence et le second sur une diagonale périphérique. Le seuil optimal $t$ correspond à une vallée du premier histogramme et à un pic du second.

	% histogramme 2D - Fisher
	\paragraph{Histogramme 2D - Fisher Linear Discreminant}
On peut utiliser le discriminant de Fisher - Fisher Linear Discreminant [\ref{fischer}], sur un histogramme 2D, pour trouver un seul optimal et segmentaire une image \cite{fisher-histo-seuil}.

	% histogramme 2D - entropie
	\paragraph{Histogramme 2D - Entropie}
Les histogrammes 2D [\ref{histogramme2D}] permettent, grâce aux calculs de l'entropie, de trouver un seuil optimal pour une segmentation d'image.
\\On divise l'histogramme 2D $H$, grâce à deux seuils $t$ et $s$.
\\ $t$ $\in$ "couleurs de l'image" et $S$ $\in$ "moyennes de couleur dans un voisinage".
\\Le seuil optimal est trouvé en minimisant l'incertitude au sein des classes $Hb(t,s)$ ($H[0:t,0:t]$) et $Hf(t,s)$ ($H[t:end,s:end]$) \cite{entropy-histo-seuil} :
\begin{gather*}
max(min(Hb(t,s),Hf(t,s))) \\
Hb(t,s) = \sum\limits_{i=0}^{t} \sum\limits_{j=0}^{s} \frac{H(i,j)}{H(t,s)} \\
Hf(t,s) = \sum\limits_{i=t}^{end} \sum\limits_{j=s}^{end} \frac{H(i,j)}{H(t,s)}
\end{gather*}
Une autre méthode utilisant l'entropie consiste à diviser l'histogramme 2D en quatre blocs, puis à maximiser la somme des incertitudes des blocs \cite{entropy2-histo-seuil}.

	% histogramme 2D - entropie floue
	\paragraph{Histogramme 2D - Entropie floue}
L'utilisation de l'entropie floue et de l’histogramme 2D [\ref{histogramme2D}] permet de trouver les seuils optimaux pour la segmentation d'images \cite{entropy-flou1-seuil}. 
\\Dans le but de classifier les pixels en deux groupes, on utilise deux fonction d'appartenance $\mu_A(\cdot)$ et $\mu_B(\cdot)$. Ces fonctions utilisent les paramètres $a$, $b$ et $c$. 
$\mu_A(\cdot)$ est définis par :
\[\mu_A(x) = \left\{ 
\begin{array}{l l}
  0 \quad \text{si x $\leq$ a}\\
  \frac{(x-a)^2}{(b-a)(c-a)} \quad \text{si a $\leq$ x $\leq$ b}\\
  1 - \frac{(x-c)^2}{(c-b)(c-a)} \quad \text{si b $\leq$ x $\leq$ c}\\
  1 \quad \text{si c  $\leq$ x}\\ 
\end{array} 
\right. \]
$\mu_B(\cdot)$ est défini par :
$$
\mu_B(x) = 1 - \mu_A(x)
$$
On utilise, ensuite, l'entropie floue $H(\cdot)$ pour quantifier la quantité d'information des classes $A$ et $B$. 
\begin{gather*}
H(A) = \sum\limits_{i=0} \mu_A(x_i)p(x_i)log(p(x_i)) \\
H(B) = \sum\limits_{i=0} \mu_B(x_i)p(x_i)log(p(x_i))
\end{gather*}
Le seuil optimal est trouvé en maximisant la somme $H(A) + H(B)$, tout en itérant sur les paramètres $a$ et $c$. Le paramètre $b$ est défini par : $(c+a)/2$.
\\
\\Une seconde technique utilisant $\mu_A(x)$ et $\mu_B(x)$ consiste à séparer les pixels en quatre classes $\mu_{Ax}$, $\mu_{Bx}$, $\mu_{Ay}$, $\mu_{By}$ et à appliquer une fonction sommant une partie entropique floue et une partie entropique non-floue. Le seuil optimal est trouvé en maximisant la fonction d'entropie \cite{entropy-flou2-seuil}.

	% blocs fixes
	\paragraph{Blocs fixes}\label{blocsFixes}
Pour la méthode des blocs fixes, on se sert du calcule de l'entropie sur des blocs de l'image binaire \cite{bloc-fixed-seuil}.
\\On sépare l'image segmenté en blocs de taille $[s \times s]$, puis on les classifie en analysant leurs structures. La matrice de classification est de taille $2^{s^2}$.
\\Le seuil, qui maximise l'incertitude de la matrice de classification, est le seuil optimal. Sa recherche est faite en itérant sur l'espace des seuils et sur l'espace des tailles de blocs.

\begingroup
    \centering
    \includegraphics[scale=0.8]{../img/blocsFixed.png}
    \captionof{figure}{Exemple de classes possibles pour les blocs de taille $[2 \times 2]$ \cite{bloc-fixed-seuil}.}
\endgroup

	% blocs mouvants
	\paragraph{Blocs mouvants}
La méthode des blocs mouvants ressemble à la méthode des blocs fixes [\ref{blocsFixes}] de par le calcule de l'entropie sur des blocs de l'image segmentée \cite{bloc-fixed-seuil}.
\\Au sein de cette méthode, on crée une fenêtre de taille $[s \times s]$ que l'on déplace sur l'image et qui permet, pour chaque position, de récupérer un bloc à classifier. La classification se fait sur le nombre de pixels blancs au sein du bloc.
\\Le seuil idéale est trouvé en maximisant la fraction suivante :
$$
\frac{\text{entropie de la matrice de classification de taille} [s \times s]}{\text{entropie max pour les blocs de taille }[s \times s]}
$$
L'entropie max pour les blocs de taille $[s \times s]$ est égale à $log(s^2+1)$.
\\La recherche du seuil est réalisée en itérant sur l'espace des seuils et sur l'espace des tailles de blocs.

	% ensembles aléatoires
	\paragraph{Ensembles aléatoires}
Cette méthode est basé sur l'analyse des distributions d'ensembles de variables aléatoires \cite{randomSet-seuil}.
\\Les outils mathématiques utilisés au sein de cette méthode sont l'espérance $E(\cdot)$, la distance $d(\cdot,\cdot)$ et la norme $\| \cdot \|$. Les distances peuvent être euclidiennes, signées et carrées. Les normes 1, 2, et infini peuvent être utilisées.
\\L'idée principale, pour une image $I$, est de calculer, pour chaque seuil $t$, une fonction de distance $d(I,t)$ et de trouver ensuite l'espérance des distances totales :
\begin{gather*}
\overline{d}(\cdot) = E(d(\cdot,t)) \\
t \in [T_{min},T_{max}]
\end{gather*}
Le seuil optimal $t'$ est défini comme étant le seuil qui minimise l'expression ci-dessous :
$$
\| \overline{d}(\cdot) - d(\cdot , t') \|
$$

\section{Variation du seuil local}
Les méthodes de variation du seuil local sont des méthodes travaillant sur la notion de voisinage.

	% contraste local
	\paragraph{Contraste locale}
Dans la méthode de contraste locale, chaque pixel, possède son propre seuil. Ce dernier est calculé en moyennant les intensité de couleur dans un voisinage de taille $[15\times 15]$ \cite{survey-seuil-tot}.

	% surface de fitting
	\paragraph{Surface de fitting}
La méthode de surface de fitting utilise un filtre passe-bas, un calcule de gradient et une interpolation surfacique \cite{surface-seuil}.
\\Le filtre passe-bas permet d'améliorer le résultat du gradient $G$, en lissant l'image originale. Un seuillage est ensuite opérer sur le gradient, permettant ainsi de séparer deux groupes de pixels, les pixels intra-objets et les pixels de contours des objets. Un post-processing est finalement appliqué aux surfaces contenant les pixels intra-objets. Les tailles et les intensité de couleur moyenne sont alors analysés.

\begingroup
    \centering
    \includegraphics[scale=0.7]{../img/Surface1.png}
    \captionof{figure}{Image originale \cite{surface-seuil}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.7]{../img/Surface2.png}
    \captionof{figure}{Seuillage du gradient $G$ \cite{surface-seuil}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.7]{../img/Surface3.png}
    \captionof{figure}{Seuillage de l'image originale grâce à la surface calculée \cite{surface-seuil}.}
\endgroup


	% niveau de gris locale
	\paragraph{Niveau de gris locale}
La méthode analysant le niveau de gris locale opère grâce à une fenêtre de voisinage de taille $[5 \times 5]$ \cite{gris-local-seuil}.
\\La fenêtre de voisinage $F(\cdot)$ ne considère que les pixels marqués par un $*$ pour l'analyse :
$$ 
F(x_i) =
\begin{pmatrix}
  0 * * * 0 \\
  * * 0 * * \\
  * 0 x_i 0 * \\
  * * 0 * * \\
  0 * * * 0 \\
 \end{pmatrix}
 $$
Seul la valeur maximale $n_i$ est utilisée dans le calcule du seuillage : $n_i = max(F(x_i))$.
\\La fonction de classification applique un seuillage non-linéaire sur l'image $I$, en utilisant deux paramètres $\mu$ et $\tau$. 
\\Si $n_i$ $\leq$ 40 :
\[x_i = \left\{ 
\begin{array}{l l}
  1 \quad \text{si } x_i - n_i < \tau \\
  0 \quad \text{sinon} \\
\end{array} 
\right. \]
Si $n_i$ > 40 :
\[x_i = \left\{ 
\begin{array}{l l}
  1 \quad \text{si } x_i < \frac{n_i}{\mu} \\
  0 \quad \text{sinon} \\
\end{array} 
\right. \]

	% approximation Gaussienne
	\paragraph{Approximation Gaussienne}
La méthode d'approximation Gaussienne utilise une division de l'image en blocs, un histogramme par blocs et des approximations Gaussiennes \cite{histo-gauss-seuil}.
\\Les blocs sont des fenêtres de taille $[7 \times 7]$. Pour chaque bloc, un histogramme est calculé. Dans le cas où l'histogramme est bimodal, on l'approxime par la somme de deux Gaussiennes. Le seuil est choisis de manière à  minimiser l'erreur de classification et est valable pour tous les pixels de la fenêtre.
\\Dans le cas où l'histogramme n'est pas bimodal, on calcule le seuil par interpolation des seuils voisins.

\begingroup
    \centering
    \includegraphics[scale=0.6]{../img/bigaussian.png}
    \captionof{figure}{Approximation bi-Gaussienne.}
\endgroup

	% entropie locale
	\paragraph{Entropie Locale}
La méthode de l'entropie locale utilise le calcule de l'entropie sur des blocs de voisinage et un filtre passe-bas \cite{survey-seuil-tot}.
\\L'entropie se calcule grâce à la formule de Yen ou de Pun [\ref{entropie}]. Pour chaque bloc, le seuil optimal correspond au maxima de l'entropie.
\\A la suite du seuillage, on applique un filtre passe-bas à la frontière des blocs.

	% méthode de la relaxation
	\paragraph{Méthode de la relaxation}
La méthode de la relaxation classifie les pixels en utilisant le principe de convergence, de probabilité d'appartenance à une classe et d'interpolation par moyenne du voisinage \cite{survey-seuil-tot}.
\\Dans un premier temps, on calcule les probabilités des pixels de l'image $I$. Pour cela on se sert des paramètres $D = min(min(I))$ et $L = max(max(I))$. On calcule ensuite, pour chaque pixels $x$, les probabilités $P_D$ et $P_L$ d'appartenir aux groupes $D$ et $L$ :
\begin{gather*}
P_D(x) = \frac{L-x}{L-D} \\
P_L(x) = \frac{G-x}{L-D}
\end{gather*}
Il est également possible de classifier les pixels en utilisant le paramètre $M = mean(mean(I))$ :
\begin{gather*}
P_D(x) = \frac{1}{2} + \frac{M-x}{2(M-D)} \\
P_L(x) = \frac{1}{2} + \frac{x-M}{2(L-M)}
\end{gather*}
On remplace ensuite les valeurs de probabilité par les moyennes de probabilité du voisinage. On itère cette étape jusqu'à convergence \cite{relaxation-seuil} et on applique un seuillage en tenant compte des probabilités calculés. Il est également possible d’arrêter les itérations lorsqu'on maximise l'information mutuelle des deux classes \cite{relaxation2-seuil} :
$$
I(P_D,P_L) = \sum\limits_{x \in I} P_D(x)log((\frac{P_D(x)}{P_L(x)})
$$

	% algorithme de Lee
	\paragraph{Algorithme de Lee}
L'algorithme de Lee permet de trouver un chemin séparant deux objets au sein d'une image \cite{lee}.
\\Lorsque les algorithmes de classification [\ref{classifieurs}] ne permettent pas la séparation de régions d'intensités de couleurs similaires, il est possible de trouver une droite ou une courbe permettant la segmentation.
\\Pour commencer, on récupère, grossièrement, la partie de l'image où se trouve le chemin à trouver. On applique ensuite itérativement un seuil sur cette région, jusqu'à obtenir un chemin séparant les régions. Des contraintes de longueur ou de courbure permettent d'optimiser le chemin trouvé.

\begingroup
    \centering
    \includegraphics[scale=0.4]{../img/LeeExemple1.png}
    \captionof{figure}{Exemple d'image nécessitant l'utilisation de l'algorithme de Lee pour la segmentation \cite{lee}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.65]{../img/LeeExemple2.png}
    \captionof{figure}{Schéma représentant les différents chemins trouvés par l'algorithme de Lee \cite{lee}.}
\endgroup

\section{Exemples d'applications : Segmentation Osseuse}\label{osExemples}
A partir des images de tomodensitométrie, l'os est un organe facilement segmentable par seuillage. En effet, au niveau de ces images, les parties osseuses sont hyper-intenses et se démarquent facilement des tissus environnants.
\\
\\Dans le cas où l'on souhaite opérer une reconstruction à 3 dimension par la suite, il est possible d'utiliser un algorithme de seuillage itératif à variation locale \cite{seuillageExemple1}. 
\\On opère un seuillage sur l'image originale et on classifie les pixels de l'image seuillée comme étant soit osseux ou soit non-osseux. Les deux classes sont approximées par deux Gaussiennes, puis les classes sont mis à jour en utilisant le voisinage des pixels et la règle de \textit{Bayes}. L'approximation Gaussienne et la re-classification sont réitérées jusqu'à la stabilisation des classes de pixels. La classification peut également être améliorée en utilisant des contraintes ou des règles propres à l'os segmenté.

\begingroup
    \centering
    \includegraphics[scale=0.55]{../img/seuillageOsExemple1.png}
    \captionof{figure}{Exemple de segmentation osseuse. Le contour rose est tracé manuellement. Le contour bleu est calculé par l'algorithme itératif \cite{seuillageExemple1}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.6]{../img/seuillageOsExemple2.png}
    \captionof{figure}{Exemple d'images segmentées puis reconstruites en 3 dimensions \cite{seuillageExemple1}.}
\endgroup

Dans le cas où on s'intéresse à la qualité des os ou à un diagnostique d'ostéoporose, il est possible d'utiliser un seuillage sur des images de micro-tomographie. L'ostéoporose est une maladie caractérisée par une diminution de la masse osseuse. La micro-tomographie est une modalité d'imagerie très précise permettant, dans le cas de l'os, d'analyser tissus trabéculaire \cite{seuillageExemple2}. Le diagnostique d' ostéoporose est basé, en partie, sur l'épaisseur des trabécules. 
\\La difficulté de la segmentation, du tissu trabéculaire, réside dans le choix du seuil optimal. En effet, différents seuils modifient l'épaisseur des trabécules, sans augmenter les erreurs de classification, faciles à reconnaître. Ceci peut entrainer des erreurs diagnostiques. Une équation d'index de modèle structurel - \textit{structural model index} (SMI) permet, lorsqu'on la maximise, de trouver le seuil optimal.

\begingroup
    \centering
    \includegraphics[scale=0.9]{../img/seuillageTrabeculeExemple.png}
    \captionof{figure}{Segmentation du tissus trabéculaire osseux par l'utilisation de quatre seuils différents. Chaque seuil modifie l'épaisseur des trabécules \cite{seuillageExemple2}.}
\endgroup