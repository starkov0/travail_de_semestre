\chapter{Analyse de texture}\label{texture}
L'analyse de texture est une méthode utilisée dans la segmentation d'images. Elle permet de classer ou de regrouper plusieurs parties d'une image, entre elles, par ressemblance.
\\Les types d'analyse les plus courants sont l'analyse structurelle et l'analyse statistique. 
A la suite de l'analyse, on se sert souvent de classifieurs [\ref{classifieurs}] pour segmenter les images. 

\section{Analyse structurelle}
L'analyse structurelle s'intéresse aux règles d'arrangement des pixels entre eux.
\\En passant par une analyse de la dimension fractale [\ref{fractal}], il est possible de déterminer les règles structurelles des textures. Il existe de nombreuses méthodes pour le calcule de cette dimensions \cite{fractal-analysis-texture}. Deux sont présentés ci-dessous.
\\Lorsque la dimension est trouvée, on divise l'image en blocs et on calcule la dimension locale. La segmentation de l'image est basée sur la comparaison entre la dimension locale et la dimension "$D$". Les blocs sont soit chevauchants, soit non-chevauchants.

	% box counting
	\paragraph{Box-counting}
Cette méthode calcule la dimension fractale par la méthode de recouvrement par objets géométriques \cite{fractal-analysis-texture}.
\\Les objets géométriques utilisées sont des carrées de taille "$r$". Pour chaque échelle "$h$", on calcule la dimension "$D_h$" et on applique ensuite une régression linéaire afin de trouver la dimension "$D$".

	% mouvement Brownienne
	\paragraph{Mouvement Brownien}
Cette méthode utilise une fonction de mouvement brownien fractale pour le calcule de la dimension fractale \cite{fractal-brownien-texture}.
\\Le mouvement brownien est un mouvement dit aléatoire. Le mouvement brownien fractal est un mouvement aléatoire, basé sur une fonction aléatoire "$I()$" non-uniforme. La fonction cumulative du mouvement brownien fractale "$F()$" est défini par :
$$
P(\frac{I(x+\Delta x}{\| \Delta x\|^H}) = F(y)
$$
La dimension est ensuite calculée grâce à\footnote{Dans le cas où I(x) est un scalaire} :
$$
D = 2 - H
$$
"$H$" est soit calculé grâce à la fonction cumulative "$F()$", soit grâce à la fonction de densité spectrale de puissance "$P(f)$" de la fonction aléatoire "$I(x)$".
$$
P(f) = f^{-2H-1}
$$

\section{Analyse statistique}
L'analyse statistique s'intéresse aux relations entre pixels. Les relations peuvent être analysées par des méthodes statistiques ou spectrales.

	% classification statistique
	\paragraph{Classificaiton statistique}
Lors de cette méthode, on divise l'image en blocs et pour chaque bloc on crée un vecteur de classification. On se sert ensuite de classifieurs mathématiques [\ref{classifieurs}] pour la segmentation.
\\Les vecteurs sont habituellement composés de 14 variables statistiques\footnote{second moment angulaire, contraste, corrélation, somme des carrés, moment inverse de la différence, moyenne des sommes, variance des sommes, entropie des sommes, entropie, différence de variance, différence d'entropie, mesure d'information de corrélation, coefficient maximal de corrélation} \cite{features-texture}. Il est possible d'inclure des variables tirées d'histogrammes 2D [\ref{histogramme2D}] ou de matrices de cooccurrence [\ref{cooccurrence}].

Dans le but d'avoir des variables statistiquement significatives, on utilise volontiers des blocs de grande taille pour classifier soit le bloc lui-même, soit le pixel central du bloc. Plus la taille des blocs augmente, plus des erreurs de classification, aux contours des objets, apparaissent. Il existe plusieurs méthodes permettant de résoudre ce problème \cite{hist-coocc-texture-hopfield}. :
\begin{itemize}[$\bullet$]
\item On effectue un post-processing sur l'image où l'on définit les contours des objets comme étant soit des droites, soit des courbes.
\item On division chaque bloc en 5 sous-blocs se chevauchant\footnote{quatre sous-blocs quadrants et un sous-bloc central}. Lorsque au moins un des sous-blocs fait entièrement partie de l'objet, on classifie le pixel central comme faisant partie de l'objet.
\item On divise chaque bloc en quatre sous-blocs, et on divise chaque sous-bloc en quatre sous-sous-blocs. On calcule la moyenne “$AV$“ de chacun des sous-sous-bloc, puis on calcule l'index d'homogénéité "$HI$" de chacun des sous-bloc grâce aux valeurs des moyennes des sous-sous-blocs
\begin{align*}
& HI = \abs{(AV1+AV4)-(AV2+AV3)} \\
& +\abs{(AV3+AV4)-(AV1+AV2)}
\end{align*}
Le sous-bloc, dont le "$HI$" est le plus bas, est utilisé pour classifier le pixel central du bloc.

\begingroup
    \centering
    \includegraphics[scale=1.0]{../img/matrice54.png}
    \captionof{figure}{Division des blocs en sous-blocs et en sous-sous-blocs \cite{hist-coocc-texture-hopfield}.}
\endgroup

\item On utilise des blocs de taille variable. De grands blocs à l'intérieur des objets et des petits blocs au niveau des contours.\end{itemize}

	% classification spectrale
	\paragraph{Classification spectrale}
La classification spectrale est basée sur plusieurs méthodes. Fourrier [\ref{fourrier}], Wavelette [\ref{wavelette}] et Gabor [\ref{gabor}] sont les plus populaires. Chacune de ces méthodes est basé sur la création d' un ensemble de fonctions puis de la projection de ces fonctions sur l'image à segmenter. 

	% Fourrier 
	\paragraph{Fourrier - segmentation}
La décomposition en série de Fourrier [\ref{fourrier}] se fait grâce à l'algorithme de transformée de fourrier rapide. 
\\Pour la segmentation, on réalise une analyse locale basé soit sur la phase de la transformée \cite{fourrier-phase-texture}, soit sur des histogramme de coefficients de fourrier \cite{fourrier-histo-texture}. Finalement, un algorithme de clustering \ref{kMoyennes} est utilisé.

	% Wavelette 
	\paragraph{Wavelette - segmentation}
On crée des vecteurs de classification pour chaque pixel de l'image, en se basant sur des donnés de voisinage de la transformée en wavelette [\ref{wavelette}]. On utilise ensuite un algorithme de classification [\ref{classifieurs}] pour segmenter l'image \cite{wavelette-texture}.

	% segmentation - Gabor
	\paragraph{Gabor - segmentation}
On crée $\tau log_2(Nc/2)$ filtres de Gabor [\ref{gabor}]. "$\tau$" est le nombre de rotations que l'on souhaite analyser et "$Nc$" est la taille de l'image. 
\\Une convolution est appliquée entre l'image et chacun des filtres, puis un filtre passe-bas est appliqué aux images résultantes. L'extraction des maximas de ces dernières permet de localiser les textures de l'image. L'utilisation d'un algorithme de classification \ref{classifieurs} permet la segmentation de l'image \cite{gabor1-texture} \cite{gabor2-texture} \cite{gabor3-texture}.

% exemples
\section{Exemples d'applications : Classification des carcinomes pulmonaires}\label{cancerExemple}
L'analyse de texture est utilisée dans la classification de cancers, et plus particulièrement dans les carcinomes pulmonaires. Un carcinome est un tissu cancérigène provenant d'un tissu épithélial. La modalité utilisée pour l'acquisition des images est le PET/CT. Le PET/IRM n'a pas été testée mais devrait, à priori, pouvoir également être utilisé.
\\
\\Pour cette classification, on utilise des classifieurs [\ref{classifieurs}], comme par exemple des arbres de décision [\ref{arbre}], des réseaux de neurones de type Perceptron [\ref{perceptron}], des Support Vector Machines [\ref{SVM}] ou un algorithme des k-moyennes [\ref{kMoyennes}]. 
\\Les vecteurs de classifications sont remplis de nombreuses données caractérisant les régions d'intérêt \cite{textureExemple1}. 
\\Les plus importantes sont les statistiques de première ordre trouvées grâce à un histogramme, les statistiques de second ordre trouvée grâce à une matrice de cooccurrence, les statistiques de haut niveau grâce à une matrice de différence de niveau de gris dans le voisinage - Neighborhood Grey-Tone Difference Matrices (NDGTMs), les données provenant d'analyses structurelles et des données quantifiant la perception humaine grâce aux \textit{Tamura Features}. 
\\La matrice NDGTMs est une matrice très semblable à un histogramme à 2 dimensions. Dans l'histogramme 2D, les entrées sont les couleurs des pixels et les moyennes de couleurs dans un voisinage proche des pixels. Dans la matrice NDGTMs, les entrées sont les couleurs des pixels et les moyennes de couleurs dans un voisinage élargis.
\\Les Tamura Features sont des données incluant la direction, la rugosité, le contraste, la grossièreté, la régularité et la ressemblance des lignes de l'image.

\begingroup
    \centering
    \includegraphics[scale=0.4]{../img/Texture1.png}
    \captionof{figure}{Données caractéristiques utilisées par les vecteurs de classification \cite{fractalDimentionExample}.}
\endgroup

\begingroup
    \centering
    \includegraphics[scale=0.4]{../img/Texture2.png}
    \captionof{figure}{Résultat de la classification. En rouge, la classification par classifier automatique, en vert la classification manuelle \cite{fractalDimentionExample}.}
\endgroup